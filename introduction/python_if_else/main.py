

inputnumber = int(input())

if inputnumber % 2 != 0:
    print("Weird")
elif inputnumber % 2 == 0 and 2 <= inputnumber <=5:
    print("Not Weird")
elif inputnumber % 2 == 0 and 6 <= inputnumber <= 20:
    print("Weird")
elif inputnumber % 2 == 0 and inputnumber > 20:
    print("Not Weird")
else:
    print("Hello Youtube")